﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_csharp_hw7
{
    public class DataLoaderThread: IDataLoader
    {

        private int CountThread;
        private long TimeLoad;

        public DataLoaderThread(int countThread)
        {
            CountThread = countThread;
        }

        public void Load(IDataSerializer dataSerializer, string dataFile, int countDataRows, ICustomerRepository customerRepository) {

            var stopwatch = new Stopwatch();
            stopwatch.Start();

            Thread[] threads = new Thread[CountThread];
            ThreadProcessingHandler[] Handlers = new ThreadProcessingHandler[CountThread];
            for (int i = 0; i < threads.Length; i++)
            {

                Handlers[i] = new ThreadProcessingHandler(i + 1, dataSerializer, dataFile, (i * countDataRows / CountThread) + 1, countDataRows / CountThread, customerRepository);
                threads[i] = new Thread(Handlers[i].Handle);
                threads[i].Start();
            }

            for (int i = 0; i < threads.Length; i++)
            {
                threads[i].Join();
            }

            stopwatch.Stop();
            TimeLoad = stopwatch.ElapsedMilliseconds;
        }

        public string ShowInfoWork()
        {
            return  $"Время на загрузку с помощью {CountThread} потоков (Thread): {TimeLoad} ms.";
        }

    }
}
