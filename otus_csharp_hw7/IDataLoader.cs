﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_csharp_hw7
{
    public interface IDataLoader
    {
        public void Load(IDataSerializer dataSerializer, string dataFile, int countDataRows, ICustomerRepository customerRepository);
        public string ShowInfoWork();
    }
}
