﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_csharp_hw7.Models
{
    public class Customer
    {
        public int Id { get; init; }
       
        [Required]
        [MaxLength(150)]
        public string? FullName { get; set; }
        [Required]
        [MaxLength(50)]
        public string? Email { get; set; }
        [Required]
        [MaxLength(20)]
        public string? Phone { get; set; }

        public Customer() { }

        public Customer(int id, string fullName, string email, string phone)
        {

            Id = id;
            FullName = fullName;
            Email = email;
            Phone = phone;
        }

        public override string ToString()
        {
            return $"Id: {Id}, FullName: {FullName}, Email: {Email}, Phone: {Phone}";
        }


    }
}
