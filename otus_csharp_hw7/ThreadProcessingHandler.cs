﻿using otus_csharp_hw7.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_csharp_hw7
{

    public class ThreadProcessingHandler
    {
        private int NumberThread;
        private int StartLine;
        private int CountLines;
        private string FileData;
        private IDataSerializer DataSerializer;
        private ICustomerRepository CustomerRepository;
        public ManualResetEvent doneEvent;

        public ThreadProcessingHandler(int numberThread, IDataSerializer dataSerializer, string fileData, int startLine, int countLines, ICustomerRepository customerRepository)
        {
            NumberThread = numberThread;
            StartLine = startLine;
            CountLines = countLines;
            FileData = fileData;
            DataSerializer = dataSerializer;
            doneEvent = new ManualResetEvent(false);
            CustomerRepository = customerRepository;
        }

        public void Handle()
        {
            Console.WriteLine($"Поток {NumberThread}. Десериализация файла...");

            List<Customer> Customers = DataSerializer.DeserializeList<Customer>(FileData, StartLine, CountLines);

            Console.WriteLine($"Поток {NumberThread}. Запись в БД...");
            CustomerRepository.AddCustumers(NumberThread, Customers);

            Console.WriteLine($"Поток {NumberThread}. Обработка завершена!");
            doneEvent.Set();
        }
    }

}
