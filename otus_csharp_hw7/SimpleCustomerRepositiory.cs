﻿using otus_csharp_hw7.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_csharp_hw7
{
    public class SimpleCustomerRepositiory : ICustomerRepository
    {
        public void StartMigration()
        {
            // Миграция данных
        }

        public void StartInitialization()
        {
            // Инициализация
        }

        public void AddCustumer(int numberThread, Customer customer)
        {
            // Эмитация записи
            System.Threading.Thread.Sleep(1);
        }

        public void AddCustumers(int numberThread, List<Customer> customers)
        {
            // Эмитация записи
            // Порционная загрузка по N записей
            int N = 1000;
            int i = 0;
            List<Customer> part_customers = new List<Customer>();
            foreach (Customer customer in customers)
            {
                part_customers.Add(customer);
                i++;
                if (i == N)
                {
                    // Загрузка порции
                    System.Threading.Thread.Sleep(1);
                    part_customers.Clear();
                    i = 0;
                }
            }
            if (i != 0)
            {
                // Загрузка последней порции
                System.Threading.Thread.Sleep(1);
                part_customers.Clear();
                i = 0;
            }
        }
    }
}
