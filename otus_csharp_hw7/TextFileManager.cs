﻿using otus_csharp_hw7.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_csharp_hw7
{
    public static class TextFileManager
    {
        public static void Write(string fullFileName, string fileText)
        {
            try
            {
                StreamWriter sw = new StreamWriter(fullFileName, false);
                sw.Write(fileText);
                sw.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e.Message);
                throw new FieldAccessException();
            }
            finally
            {   
            }
        }

        public static string Read(string fullFileName)
        {

            try
            {
                StreamReader sr = new StreamReader(fullFileName);
                string fileData = sr.ReadToEnd();
                sr.Close();
                return fileData;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e.Message);
                throw new FieldAccessException();
            }
            finally
            {
            }

        }
    }
}
