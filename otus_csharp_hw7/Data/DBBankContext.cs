﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using otus_csharp_hw7.Models;

namespace otus_csharp_hw7.Data
{
    public class DBBankContext : DbContext
    {
        public DBBankContext(DbContextOptions<DBBankContext> options)
            : base(options)
        {
        }

        public void EnsureUpdate()
        {
            Database.EnsureDeleted();   // удаляем бд со старой схемой
            Database.EnsureCreated();   // создаем бд с новой схемой
        }

        public DbSet<Customer> Customers { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>().ToTable("Customers");
        }

    }

}
