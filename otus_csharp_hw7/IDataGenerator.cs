﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_csharp_hw7
{
    public interface IDataGenerator<T> where T : class
    {
        public string GenerateText(IDataSerializer dataSerialazer, int countRows);
        public List<T> GenerateList(int count);
        public T Generate(int id);
    }
}
