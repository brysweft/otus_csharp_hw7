﻿using Microsoft.EntityFrameworkCore;
using otus_csharp_hw7.Data;
using otus_csharp_hw7.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_csharp_hw7
{
    public class DBCustomerRepositiory : ICustomerRepository
    {

        private DBBankContext[] contexts;
        private SampleContextFactory contextFactory;

        public DBCustomerRepositiory(int countThread) {


            contextFactory = new SampleContextFactory();
            string[] args = new string[0];
            contexts = new DBBankContext[countThread];
            for (int i = 0; i < countThread; i++) {
                contexts[i] = contextFactory.CreateDbContext(args);
            }

            StartInitialization();

        }

        public void StartMigration()
        {
            Console.WriteLine("Миграция данных БД...");
            contexts[0].Database.Migrate();
            Console.WriteLine("Миграция данных БД завершена.");
        }

        public void StartInitialization()
        {
            Console.WriteLine("Инициализация БД...");
            contexts[0].EnsureUpdate();
            DbInitializer.Initialize(contexts[0]);
            Console.WriteLine("Инициализация БД завершена.");
        }

        public void AddCustumer(int numberThread, Customer customer)
        {
            contexts[numberThread-1].Customers.Add(customer);
            contexts[numberThread - 1].SaveChanges();
        }

        public void AddCustumers(int numberThread, List<Customer> customers)
        {

            // Порционная загрузка по N записей
            int N = 1000;
            int i = 0;
            List<Customer> part_customers = new List<Customer>();
            foreach (Customer customer in customers)
            {
                part_customers.Add(customer);
                i++;
                if (i == N)
                {
                    // Загрузка порции
                    contexts[numberThread - 1].Customers.AddRange(part_customers.ToArray());
                    contexts[numberThread - 1].SaveChanges();
                    part_customers.Clear();
                    i = 0;
                }
            }
            if (i != 0)
            {
                // Загрузка последней порции
                contexts[numberThread - 1].Customers.AddRange(part_customers.ToArray());
                contexts[numberThread - 1].SaveChanges();
                part_customers.Clear();
                i = 0;
            }
            
        }

    }
}
