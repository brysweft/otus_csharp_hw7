﻿using otus_csharp_hw7.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_csharp_hw7
{
    public interface ICustomerRepository
    {
        public void StartMigration() { }

        public void StartInitialization() { }

        public void AddCustumer(int numberThread, Customer customer) { }

        public void AddCustumers(int numberThread, List<Customer> customers) { }
    }
}
