﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_csharp_hw7
{
    public interface IDataSerializer
    {
        public string Serialize<T>(List<T> listObjects) where T : class;
        public string Serialize<T>(T obj) where T : class;
        public List<T> DeserializeList<T>(string description, int startLine = 1, int countLines = 0) where T : class, new();
        public T Deserialize<T>(string description) where T : class, new();

    }
}
