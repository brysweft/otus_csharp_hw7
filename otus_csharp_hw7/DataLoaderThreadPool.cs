﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_csharp_hw7
{
    public class DataLoaderThreadPool : IDataLoader
    {

        private int CountThread;
        private long TimeLoad;

        public DataLoaderThreadPool(int countThread)
        {
            CountThread = countThread;
        }

        public void Load(IDataSerializer dataSerializer, string dataFile, int countDataRows, ICustomerRepository customerRepository)
        {

            var stopwatch = new Stopwatch();
            stopwatch.Start();

            ThreadPool.SetMaxThreads(CountThread, CountThread);

            List<ThreadProcessingHandler> Handlers = new List<ThreadProcessingHandler>();


            for (int i = 0; i < CountThread; i++)
            {
                Handlers.Add(new ThreadProcessingHandler(i + 1, dataSerializer, dataFile, (i * countDataRows / CountThread) + 1, countDataRows / CountThread, customerRepository));
                ThreadPool.QueueUserWorkItem(HandlerInThreadPool, Handlers[i]);
            }

            var doneEvents = Handlers.Select(x => x.doneEvent).ToArray();
            WaitHandle.WaitAll(doneEvents);

            stopwatch.Stop();
            TimeLoad = stopwatch.ElapsedMilliseconds;
        }

        public string ShowInfoWork()
        {
            return $"Время на загрузку с помощью {CountThread} потоков (ThreadPool): {TimeLoad} ms.";
        }

        private void HandlerInThreadPool(object item)
        {
            ThreadProcessingHandler threadProcessingHandler = (ThreadProcessingHandler)item;
            threadProcessingHandler.Handle();
        }

    }
}
