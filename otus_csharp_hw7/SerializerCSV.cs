﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace otus_csharp_hw7
{
    public class SerializerCSV: IDataSerializer
    {

        private readonly string Separator = ",";

        public string Serialize<T>(List<T> listObjects) where T : class
        {

            FieldInfo[] fields = typeof(T).GetFields(BindingFlags.Public);
            PropertyInfo[] properties = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.GetProperty | BindingFlags.SetProperty | BindingFlags.NonPublic);

            var description = new StringBuilder();
            var header = string.Join(Separator, fields.Select(f => f.Name)
                .Concat(properties.Select(p => p.Name)));

            description.AppendLine(header);

            foreach (var obj in listObjects)
            {
                description.AppendLine(string.Join(Separator, fields.Select(f => f.GetValue(obj) ?? "")
                .Concat(properties.Select(p => p.GetValue(obj) ?? ""))));
            }

            return description.ToString();

        }

        public string Serialize<T>(T obj) where T : class
        {

            FieldInfo[] fields = typeof(T).GetFields(BindingFlags.Public);
            PropertyInfo[] properties = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.GetProperty | BindingFlags.SetProperty | BindingFlags.NonPublic);

            var description = new StringBuilder();
            var header = string.Join(Separator, fields.Select(f => f.Name)
                .Concat(properties.Select(p => p.Name)));

            description.AppendLine(header);

            var data = string.Join(Separator, fields.Select(f => f.GetValue(obj) ?? "")
                .Concat(properties.Select(p => p.GetValue(obj) ?? "")));

            description.AppendLine(data);

            return description.ToString();

        }

        public List<T> DeserializeList<T>(string description, int startLine = 1, int countLines = 0) where T : class, new()
        {

            List<T> ListObjects = new List<T>();

            var lines = description.Split("\r\n");

            if (lines.Length <= 1)
            {
                return ListObjects;
            }

            int CountLines = countLines == 0 ? lines.Length - startLine : countLines;

            if (CountLines <= 1 || startLine < 1)
            {
                return ListObjects;
            }

            string header = lines[0];

            string[] linesObj = new string[2];
            linesObj[0] = header;

            for (int i = startLine; i < startLine + CountLines; i++)
            {
                linesObj[1] = lines[i];
                ListObjects.Add(Deserialize<T>(string.Join("\r\n", linesObj)));
            }

            return ListObjects;

        }

        public T Deserialize<T>(string description) where T : class, new()
        {

            T obj = Activator.CreateInstance<T>();

            var lines = description.Split("\r\n");

            if (lines.Length <= 1)
            {
                return obj;
            }

            var header = lines[0].Split(Separator);
            var data = lines[1].Split(Separator);

            FieldInfo[] fields = typeof(T).GetFields();
            PropertyInfo[] properties = typeof(T).GetProperties();

            for (int i = 0; i < header.Length; i++)
            {
                var field = fields.FirstOrDefault(x => x.Name == header[i]);
                if (field != null)
                {
                    Type fieldType = field.FieldType;
                    field.SetValue(obj, Convert.ChangeType(data[i], fieldType));
                }

                var prop = properties.FirstOrDefault(x => x.Name == header[i]);
                if (prop != null)
                {
                    prop.SetValue(obj, Convert.ChangeType(data[i], prop.PropertyType));
                }
            }
            return obj;
        }
    }

}
