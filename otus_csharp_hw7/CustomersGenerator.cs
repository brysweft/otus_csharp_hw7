﻿using otus_csharp_hw7.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_csharp_hw7
{
    public class CustomersGenerator: IDataGenerator<Customer>
    {

        public string GenerateText(IDataSerializer dataSerialazer, int countRows)
        {
            List<Customer> Customers = GenerateList(countRows);
            string FileText = dataSerialazer.Serialize(Customers);

            return FileText;
        }

        public List<Customer> GenerateList(int count)
        {

            List<Customer> Customers = new List<Customer>();

            for (int i = 1; i <= count; i++) {
                Customers.Add(Generate(i));
            }

            return Customers;

        }

        public Customer Generate(int id) {

            string number = id.ToString("D7");

            return new Customer(id, "TestName" + number, "testmail" + number + "@mail.ru", "+7 (900) " + number);
        
        }
    }
}
