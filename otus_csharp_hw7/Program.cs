﻿using otus_csharp_hw7.Models;
using System.Diagnostics;

namespace otus_csharp_hw7
{
    internal class Program
    {

        public static readonly string fullFileName = Directory.GetCurrentDirectory() + @"\customers_test1.csv";
        public static readonly string programFullFileName = Directory.GetCurrentDirectory() + @"\otus_csharp_hw7.exe";
        
        public static int countThread = 4;
        public static int countDataRows = 1000;
        public static IDataLoader? dataLoader;
        public static IDataSerializer? dataSerializer;
        public static ICustomerRepository? customerRepository;

        static void Main(string[] args)
        {

            if (args.Contains("/FileGeneration"))
            {
                countDataRows = int.Parse(args[1]);
                ExtraProcess();
            }
            else {
                MainProcess();
            }

        }

        public static void MainProcess()
        {
            Console.WriteLine(" *** Параллельная обработка данных ***");

            ConsoleKey choice;
            while (true)
            {
                SetSettings();

                Console.Write("Генерировать файл в отдельном процессе? (y/n):");
                choice = Console.ReadKey(true).Key;
                Console.WriteLine(choice.ToString());
                switch (choice)
                {
                    case ConsoleKey.Y: StartExtraProcess(); break;
                    default: GenerateFile(); break;
                }

                ProcessingFile();

                Console.WriteLine("Повторить загрузку с другими настройками? (y/n):");
                choice = Console.ReadKey(true).Key;
                if (choice != ConsoleKey.Y)
                {
                    break;
                }
 
            }
            Console.WriteLine("Основной процесс завершен!");

        }

        public static void ProcessingFile()
        {
            Console.WriteLine("Обработка файла. Подготовка потоков");
            Console.WriteLine($"Количество записей: {countDataRows}");

            string dataFile = TextFileManager.Read(fullFileName);
            IDataSerializer dataSerializer = new SerializerCSV();

            dataLoader.Load(dataSerializer, dataFile, countDataRows, customerRepository);

            Console.WriteLine(dataLoader.ShowInfoWork());

        }

        public static void StartExtraProcess()
        {
            Console.WriteLine("Запуск отдельного процесса ...");

            ProcessStartInfo processStartInfo = new ProcessStartInfo()
            {
                FileName   = programFullFileName,
                ArgumentList = { "/FileGeneration", countDataRows.ToString()},
                UseShellExecute = false
            };

            Process extraProcess = Process.Start(processStartInfo);
            extraProcess.WaitForExit();

            Console.WriteLine("Окончание отдельного процесса.");
            extraProcess.Kill();      
        }

        public static void ExtraProcess()
        {
            GenerateFile();
        }

        static void GenerateFile()
        {
            Console.WriteLine("Генерация файла ...");
            IDataGenerator<Customer> dataGenerator = new CustomersGenerator();
            IDataSerializer dataSerializer = new SerializerCSV();
            string fileData = dataGenerator.GenerateText(dataSerializer, countDataRows);
            TextFileManager.Write(fullFileName, fileData);
            Console.WriteLine("Файл сгенерирован!");   
        }

        static void SetSettings()
        {
            ConsoleKey choice;

            Console.WriteLine($"Количество записей в файле: {countDataRows}");
            Console.Write("Изменить количество записей в файле? (y/n):");
            choice = Console.ReadKey(true).Key;
            Console.WriteLine(choice.ToString());
            switch (choice)
            {
                case ConsoleKey.Y: SetIntValueConsole(out countDataRows);  break;
                default: break;
            }    

            Console.WriteLine($"Количество потоков: {countThread}");
            Console.Write("Изменить количество потоков? (y/n):");
            choice = Console.ReadKey(true).Key;
            Console.WriteLine(choice.ToString());
            switch (choice)
            {
                case ConsoleKey.Y: SetIntValueConsole(out countThread); break;
                default: break;
            }

            dataSerializer = new SerializerCSV();

            Console.WriteLine("Загрузка будет выполнена с помощью потоков Thread()");
            Console.Write("Использовать потоки ThreadPool? (y/n):");
            choice = Console.ReadKey(true).Key;
            Console.WriteLine(choice.ToString());
            switch (choice)
            {
                case ConsoleKey.Y: dataLoader = new DataLoaderThreadPool(countThread); break;
                default: dataLoader = new DataLoaderThread(countThread); break;
            }

            Console.Write("Выполнять загрузку данных в БД? (y/n):");
            choice = Console.ReadKey(true).Key;
            Console.WriteLine(choice.ToString());
            switch (choice)
            {
                case ConsoleKey.Y: customerRepository = new DBCustomerRepositiory(countThread); break;
                default: customerRepository = new SimpleCustomerRepositiory(); break;
            }

        }

        static void SetIntValueConsole(out int value)
        {
            while (true)
            {
                Console.Write($"Введите число: ");
                string? input = Console.ReadLine();
                if (int.TryParse(input, out int number))
                {
                    value = number;
                    Console.WriteLine();
                    return;
                }
                {
                    Console.WriteLine("Не корректное число!");
                }
            }
        }


    }
}








